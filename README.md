#Three Day Introduction to Python in German and English:

<a rel="license" href="http://creativecommons.org/licenses/by/4.0/"><img alt="Creative Commons License" style="borde
r-width:0" src="https://i.creativecommons.org/l/by/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="
license" href="http://creativecommons.org/licenses/by/4.0/">Creative Commons Attribution 4.0 International License</
a>.

## Course content

* variables and datatypes
* lists
* logic and branching
* list comprehensions
* functions
* the Python standard library
* dictionaries and I/O
* plotting


...

# Installation:

## For GWDG Students:

* Log in to https://jupyter-cloud.gwdg.de with your GWDG account
* Create a new Terminal (to the right of the Upload button "New" $\rightarrow$ "Terminal")
* Enter the following command and execute it by pressing Return:  
  `git clone https://bitbucket.org/MantiumAndre/python_basic_training`
* The cource materials are now downloaded to the file overview

##Local Installation (optional)
To run locally, it is possible to use [Pipenv](https://pipenv.pypa.io/en/latest/).  
After installing Pipenv and cloning this repo, head to the respective folder and run:
   
```
pipenv install
```   

To run Jupyter Notebooks in this virtual environment, just run:

```
pipenv run jupyter-notebook
```   

##This material was orginaly created by the following authors:
### German version

By **Jana Lasser**

* [Original repository](https://github.com/JanaLasser/python-einfuehrung) (Github)

### English version

Translated and extended by **Torsten Eckstein**, **Filippo Cosi**, **Guus Bertens** and **Johannes Schroeder-Schetel
ig**

* [Source repository](https://gitlab.gwdg.de/teckste/Python_introduction) (GWDG Gitlab)
* [Mirror of an older version](https://github.com/JanaLasser/python-introduction) (Github)




